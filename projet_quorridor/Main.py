############## PROJET NSI : Quoiridor ##############
import flet as ft
from Quoridor_v2 import *
from time import sleep
import random
Q = Quoridor()

# import des outils nécessaires et création d'une instance de Quoridor

def main(page: ft.Page):

    #################### caractéristiques de la page ####################
    
    page.title = f"""joueur {Q.tour_de_joueur} peut se déplacer ou poser un mur (Joueur 1 : {Q.j1_murs} restant(s), Joueur 2 : {Q.j2_murs} restant(s))"""
    page.window_width = 9*(80+20) + 5
    page.window_height = 9*(80+20) + 30
    page.padding = 10 # espacement au bord
    page.window_resizable = False

    #################### fonctions ####################

    def callback(c):    # fonction effectuant le mouvement des joueurs
        w = c.control
        if Q.fini == False:
            if Q.case_valide(Q.grille_dico[w.coord]):
                x1 , y1 = Q.j1_place
                x2 , y2 = Q.j2_place
                if Q.tour_de_joueur == 1:
                    grid[Q.j1_place].image_src = "mes_images/plateau/sol.png"
                    Q.deplacement(Q.grille_dico[w.coord], Q.j1_place, Q.j2_place)
                    grid[Q.j1_place].image_src = "mes_images/joueurs/j1_personnage_MB.png"
                else:
                    grid[Q.j2_place].image_src = "mes_images/plateau/sol.png"
                    Q.deplacement(Q.grille_dico[w.coord], Q.j2_place, Q.j1_place)
                    grid[Q.j2_place].image_src = "mes_images/joueurs/j2_personnage_FR.png"
                if (x1, y1) != Q.j1_place or (x2, y2) != Q.j2_place: # vérification si un mouvement a été effectué
                    Q.changer_de_tour()
                    page.title = f"""joueur {Q.tour_de_joueur} peut se déplacer ou poser un mur (Joueur 1 : {Q.j1_murs} restant(s), Joueur 2 : {Q.j2_murs} restant(s))"""
                
                if Q.partie_terminee(): # code de la fin de partie
                    Q.fini = True
                    page.title = f"""Partie terminé"""
                    if Q.vainqueur() == 1:
                        grid[Q.j1_place].image_src = "mes_images/joueurs/j1_personnage_MB_win.png"
                        grid[Q.j2_place].image_src = "mes_images/joueurs/j2_personnage_FR_lose.png"
                    else:
                        grid[Q.j1_place].image_src = "mes_images/joueurs/j1_personnage_MB_lose.png"
                        grid[Q.j2_place].image_src = "mes_images/joueurs/j2_personnage_FR_win.png"
                    page.update()
                    sleep(3)
                    for valeur in grid.values():
                        valeur.image_src = "mes_images/plateau/solfleurs.png"
                        page.update()
                    sleep(1)
                    page.clean()
                    ecran_fin = ft.Container(width=905,
                                            height=930,
                                            expand=True,
                                            image_src = f"""mes_images/autres/ecran_fin_P{Q.vainqueur()}.png""",
                                            margin = -5) 
                    page.add(ecran_fin)
                    page.update()
        page.update()
            
    def barrer(c):  # fonction effectuant la pose des murs
        w = c.control
        if Q.fini == False:
            sens = None # le sens du mur n'est pour l'instant pas connu
            if w.image_src in lst_passage and Q.peut_poser_mur(): # vérification des prérequis ainsi que pour éviter de chevaucher un mur existant
                    x, y = w.coord
                    if type(x) == float:   # le murs est horizontal, vérification pour éviter les chevauchements
                        if  y != Q.taille-1 and grid_mur[(x, y+1)].image_src in lst_passage and not(grid_mur[(x-0.5, y+0.5)].image_src not in lst_passage and grid_mur[(x+0.5, y+0.5)].image_src not in lst_passage): # modification du sens du mur pour éviter de le croiser avec un autre
                            sens = 1
                        elif y-1 >= 0 and not(grid_mur[(x-0.5, y-0.5)].image_src not in lst_passage and grid_mur[(x+0.5, y-0.5)].image_src not in lst_passage):
                            sens = -1
                        if  sens != None: # si le sens a une valeur alors le mur peut possiblement être posé
                            Q.murer(Q.grille_dico[x-0.5, y], Q.grille_dico[x+0.5, y], sens)
                            if Q.anti_blocage(Q.j1_place, 1) and Q.anti_blocage(Q.j2_place, 2): # empêche de bloquer les joueurs
                                w.image_src = random.choice(lst_murs_h)
                                grid_mur[(x, y+sens)].image_src = random.choice(lst_murs_h)
                                if Q.tour_de_joueur == 1:
                                    Q.j1_murs -= 1
                                else:
                                    Q.j2_murs -= 1
                                Q.changer_de_tour() # changement de tour une fois les actions effectuées et vérifiées
                                page.title = f"""joueur {Q.tour_de_joueur} peut se déplacer ou poser un mur (Joueur 1 : {Q.j1_murs} restant(s), Joueur 2 : {Q.j2_murs} restant(s))"""
                            else:
                                Q.demurer(Q.grille_dico[x-0.5, y], Q.grille_dico[x+0.5, y], sens)
                        
                    elif type(y) == float:  # le murs est vertical, vérification pour éviter les chevauchements
                        if x != 0 and grid_mur[(x-1, y)].image_src in lst_passage and not(grid_mur[(x-0.5, y-0.5)].image_src not in lst_passage and grid_mur[(x-0.5, y+0.5)].image_src not in lst_passage): # modification du sens du mur pour éviter de le croiser avec un autre
                            sens = 1
                        elif x-1 >= 0 and not(grid_mur[(x+0.5, y-0.5)].image_src not in lst_passage and grid_mur[(x+0.5, y+0.5)].image_src not in lst_passage):
                            sens = -1
                        if sens != None: # empêche de croiser des murs
                            Q.murer(Q.grille_dico[x, y-0.5], Q.grille_dico[x, y+0.5], sens)
                            if Q.anti_blocage(Q.j1_place, 1) and Q.anti_blocage(Q.j2_place, 2): # empêche de bloquer les joueurs
                                w.image_src = random.choice(lst_murs_v)
                                grid_mur[(x-sens, y)].image_src = random.choice(lst_murs_v)
                                if Q.tour_de_joueur == 1:
                                    Q.j1_murs -= 1
                                else:
                                    Q.j2_murs -= 1
                                Q.changer_de_tour() # changement de tour une fois les actions effectuées et vérifiées
                                page.title = f"""joueur {Q.tour_de_joueur} peut se déplacer ou poser un mur (Joueur 1 : {Q.j1_murs} restant(s), Joueur 2 : {Q.j2_murs} restant(s))"""
                            else:
                                Q.demurer(Q.grille_dico[x, y-0.5], Q.grille_dico[x, y+0.5], sens)            
        page.update()

    #################### éléments de page ####################
        
    # Créer une grille 9x9
    grid = {} # création d'un dictionnaire afin de pouvoir parcourir la grille
    grid_mur = {} # création d'un dictionnaire afin de pouvoir retrouver les murs
    lst_murs_h = ["mes_images/murs/horizontaux/1.png", "mes_images/murs/horizontaux/2.png", "mes_images/murs/horizontaux/3.png", "mes_images/murs/horizontaux/4.png"] # liste des murs horizontaux à choisir aléatoirement
    lst_murs_v = ["mes_images/murs/verticaux/1.png", "mes_images/murs/verticaux/2.png", "mes_images/murs/verticaux/3.png", "mes_images/murs/verticaux/4.png"] # liste des murs verticaux à choisir aléatoirement
    lst_passage = ["mes_images/plateau/sans_mur_1.png", "mes_images/plateau/sans_mur_2.png"]
    
    for i in range(9):
        row_controls = []  # Liste pour stocker les contrôles de la ligne de cases et murs verticaux
        inter_row_controls = [] # Liste pour stocker les contrôles de la ligne de murs horizontaux
        for j in range(9): # Ajoute des conteneurs à la liste avec des marges servants de socle aux murs
            c = ft.Container(width=80,
                    height=80,
                    image_src = "mes_images/plateau/sol.png",
                    on_click = callback,
                    alignment = ft.alignment.center,
                    margin = -5)
            c.coord = (i, j) # création d'une case avec coordonnées attitrées
            grid[c.coord] = c
            if c.coord == Q.j1_place:
                c.image_src = "mes_images/joueurs/j1_personnage_MB.png" # placement du joueur 1
            elif c.coord == Q.j2_place:
                c.image_src = "mes_images/joueurs/j2_personnage_FR.png" # placement du joueur 2
            mur_vertical = ft.Container(width=20,
                                        height=80,
                                        image_src = "mes_images/plateau/sans_mur_2.png",
                                        on_click=barrer,
                                        margin = -5)
            mur_vertical.coord = (i, j+0.5) # création d'un mur avec coordonnées attitrées
            grid_mur[mur_vertical.coord] = mur_vertical
            row_controls.append(c)
            row_controls.append(mur_vertical)

            mur_horizontal = ft.Container(width=80,
                                        height=20,
                                        image_src = "mes_images/plateau/sans_mur_1.png",
                                        on_click=barrer,
                                        margin = -5)
            mur_horizontal.coord = (i+0.5, j) # création d'un mur avec coordonnées attitrées
            grid_mur[mur_horizontal.coord] = mur_horizontal
            block = ft.Container(width=20,
                                height=20,
                                image_src = "mes_images/plateau/herbefleurs.png",
                                margin = -5)
            inter_row_controls.append(mur_horizontal)
            inter_row_controls.append(block)

        row_controls.pop()
        inter_row_controls.pop()
        row = ft.Row(controls=row_controls)  # Crée une ligne avec les contrôles (cases et murs verticaux)
        inter_row = ft.Row(controls=inter_row_controls)
        page.add(row)  # Ajoute la ligne de cases et murs verticaux à la page
        page.add(inter_row) # Ajoute la ligne de murs horizontaux à la page
    page.remove(inter_row)

ft.app(target=main)