Pour lance le jeu, exécutez Main.py

Fonctionnement du jeu:

Chaque joueur commence dans son camp avec 10 murs/buissons en poche
Au tour par tour, chacun peut soit de déplacer ou poser un mur/buisson (ATTENTION : on ne peut bloquer complètement l'adversaire ou poser un mur sur un autre mur, si vous essayer le mur ne se posera pas)
Pour déplacer, cliquez sur la case de votre choix, pour poser un mur, cliquer sur le chemin entre deux case pour y poser un mur/buisson
Le but du jeu est d'attendre le camp adverse, bon jeu !