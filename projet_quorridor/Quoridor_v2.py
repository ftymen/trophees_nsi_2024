#### Quoridor v2 ####

from structure_lineaire import File

#### modelisation grille ###########################################################################
class Case:

    def __init__(self, coord):
        self.coord = coord # les coordonnees sont sous forme de tuple
        self.voisines = set() # utilisation du set pour y rechercher rapidement les voisins

    def relier(self, autre_case):
        if autre_case not in self.voisines:
            self.voisines.add(autre_case)
            autre_case.voisines.add(self)
    
    def delier(self, autre_case):
        if autre_case in self.voisines:
            self.voisines.remove(autre_case)
            autre_case.voisines.remove(self)

class Quoridor:
    
    def __init__(self, taille = 9):
        self.taille = taille
        self.grille_dico = {(i, j) : Case((i, j)) for j in range(self.taille) for i in range(self.taille)}
        self.intialiser()
        self.j1_place = (0, self.taille // 2)  # j1 commence en haut au milieu
        self.j2_place = (self.taille - 1, self.taille // 2)  # j2 en bas au milieu
        self.j1_murs = self.taille + 1
        self.j2_murs = self.taille + 1
        self.tour_de_joueur = 1
        self.fini = False

    def intialiser(self):
        """crée les liaisons entre les cases"""
        for i in range(self.taille):
            for j in range(self.taille):
                case = self.grille_dico[(i,j)]
                for voisine in [(i, j-1),(i, j+1),(i+1, j),(i-1, j)]:
                    if voisine in self.grille_dico.keys():
                        case.relier(self.grille_dico[voisine])
    
    def reinitialiser():
        pass

    def peut_poser_mur(self):
        """vérifie le stock de mur des joueurs"""
        if self.tour_de_joueur == 1:
            return self.j1_murs != 0
        else:
            return self.j2_murs != 0

    def murer(self, case_1, case_2, n = 1):
        """pose un mur de longueur 2 cases"""
        x1, y1 = case_1.coord
        x2, y2 = case_2.coord
        if x1 == x2: # le mur est vertical
            case_1.delier(case_2)
            self.grille_dico[(x1-n, y1)].delier(self.grille_dico[(x2-n, y2)])

        else: # le mur est horizontal
            case_1.delier(case_2)
            self.grille_dico[(x1, y1+n)].delier(self.grille_dico[(x2, y2+n)])

    def demurer(self, case_1, case_2, n = 0.5):
        """enlève un mur si il ne peut pas être posé"""
        x1, y1 = case_1.coord
        x2, y2 = case_2.coord
        if x1 == x2: # le mur est vertical
            case_1.relier(case_2)
            self.grille_dico[(x1-n, y1)].relier(self.grille_dico[(x2-n, y2)])

        else: # le mur est horizontal
            case_1.relier(case_2)
            self.grille_dico[(x1, y1+n)].relier(self.grille_dico[(x2, y2+n)])
            
    def anti_blocage(self, case_joueur, num_joueur):
        """vérifie si les joueurs ont au moins 1 possibilité d'attendre leur ligne d'arrivée"""
        visites = set()
        f = File()
        f.enfiler(self.grille_dico[case_joueur])
        while not f.est_vide():
            tete = f.defiler()
            for voisine in tete.voisines:
                if voisine not in visites:
                    f.enfiler(voisine)
                    visites.add(voisine)

        if num_joueur == 1:
            arrivee = [case for coord, case in self.grille_dico.items() if coord[0] == self.taille -1]
        else:
            arrivee =  [case for coord, case in self.grille_dico.items() if coord[0] == 0]
        for case in arrivee:
            if case in visites:
                return True
        return False
    
    def case_valide(self,autre_case):
        """vérifie si autre_case est accessible"""
        if self.tour_de_joueur == 1:
            if autre_case in self.grille_dico[self.j1_place].voisines:
                return True
        else:
            if autre_case in self.grille_dico[self.j2_place].voisines:
                return True
        return False

    def deplacement(self, autre_case, joueur_se_deplacant, autre_joueur):
        """effectue le déplacement sur autre_case"""
        x1, y1 = joueur_se_deplacant
        x2, y2 = autre_joueur
        if autre_case.coord != autre_joueur:   # si la case n'est pas occupée par un autre joueur
            joueur_se_deplacant = autre_case.coord
        else:   # si la case est occupée par un autre joueur
            if x1 == x2:    # les joueurs sont sur la même ligne
                if y1 < y2 and (x2, y2+1) in self.grille_dico.keys():
                    if self.grille_dico[(x2, y2+1)] in self.grille_dico[autre_joueur].voisines:
                        joueur_se_deplacant = (x2, y2+1)
                elif y1 > y2 and (x2, y2-1) in self.grille_dico.keys():
                    if self.grille_dico[(x2, y2-1)] in self.grille_dico[autre_joueur].voisines:
                        joueur_se_deplacant = (x2, y2-1)
            else:   # les joueurs sont sur la même colonne
                if x1 < x2 and (x2+1, y2) in self.grille_dico.keys():
                    if self.grille_dico[(x2+1, y2)] in self.grille_dico[autre_joueur].voisines:
                        joueur_se_deplacant = (x2+1, y2)
                elif x1 > x2 and (x2-1, y2) in self.grille_dico.keys():
                    if self.grille_dico[(x2-1, y2)] in self.grille_dico[autre_joueur].voisines:
                        joueur_se_deplacant = (x2-1, y2)
        # Mise à jour de l'attribut correspondant à la position du joueur qui se déplace
        if self.tour_de_joueur == 1:
            self.j1_place = joueur_se_deplacant
        else:
            self.j2_place = joueur_se_deplacant

    ### gestion du cours de la partie ###
                        
    def changer_de_tour(self):
        if self.tour_de_joueur == 1:
            self.tour_de_joueur = 2
        else:
            self.tour_de_joueur = 1

    def partie_terminee(self):
        if self.j1_place in [coord_case for coord_case in self.grille_dico.keys() if coord_case[0] == self.taille-1] or self.j2_place in [coord_case for coord_case in self.grille_dico.keys() if coord_case[0] == 0]:
            return True
    
    def vainqueur(self):
        if self.j1_place in [coord_case for coord_case in self.grille_dico.keys() if coord_case[0] == self.taille-1]:
            return 1
        elif self.j2_place in [coord_case for coord_case in self.grille_dico.keys() if coord_case[0] == 0]:
            return 2
        

