# Import necessary modules and components
from kivy.config import Config
from kivy.core.window import Window
from kivy.app import App
from kivy.lang import Builder
from kivy.uix.button import Button
from kivy.uix.slider import Slider
from kivy.uix.image import Image
from kivy.uix.behaviors import DragBehavior
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.clock import Clock
from kivy.core.text import LabelBase, DEFAULT_FONT
from kivy.uix.popup import Popup
from random import randint

import ai
import core

# Set Kivy configuration options
Config.set('kivy', 'desktop', 0)  # Disable desktop-only features
Config.set('kivy', 'exit_on_escape', 1)  # Enable press "escape" to exit app
Config.set('kivy', 'keyboard_layout', 'azerty')  # Set keyboard layout to "azerty"
Config.set('input', 'mouse', 'mouse,disable_multitouch')  # Enable multiple mouse touch detection
LabelBase.register(DEFAULT_FONT, "assets/fonts/default_font.ttf")  # Set default font


class Katamino(App): # fork app into Katamino class

    """
    Katamino Game Application

    This application allows users to play the game Katamino, a puzzle game where players must fit irregular shapes into a grid.

    Methods:
        build(): Initialize the application.
        change_screen(screen): Load a new screen into the application.
        search_widgets(id): Search for widgets with the specified ID.
        start_game(game_size): Start a new game of Katamino.
        update(): Update method, currently empty.
    """

    def build(self): # startup function
        """
        Initialize the application.

        Returns:
            FloatLayout: Root layout of the application.
        """

        self.icon = 'assets/images/katamino_icon.ico' # set window icon
        
        root = FloatLayout() # set root as a FloatLayout
        root.add_widget(Builder.load_file('assets/kv/title_screen.kv')) # load title screen by default



        # Set default window size
        Window.minimum_width, Window.minimum_height = 720, 405
        Window.size = Window.minimum_width, Window.minimum_height

        # Maintain aspect ratio for clearer displaying
        aspect_ratio = 16 / 9
        def maintain_aspect_ratio(Window, width, height):
            """
            Detect changes in the window's size and resize the window accordingly to maintain aspect ratio
            """
            new_height = width / aspect_ratio
            if new_height != height:
                Window.size = (width, new_height)

        Window.bind(on_resize=maintain_aspect_ratio)



        # Key presses detection
        def key_press_detection(Window, keyboard, keycode, text, modifiers):
            """
            Detect key presses and trigger actions accordingly.
            """

            if keycode == 18 and hasattr(self, "current_piece"):
                self.current_piece.rotate(core.ORIENTATIONS[(core.ORIENTATIONS.index(self.current_piece.orientation)+1) % 8])
            if keycode == 68:
                if Window.fullscreen == 'auto':
                    Window.fullscreen = False
                else:
                    Window.fullscreen = 'auto'
            
        Window.bind(on_key_down=key_press_detection)



        return root



    def change_screen(self, screen):
        """
        Load a new screen into the application.

        Args:
            screen (str): Name of the screen to load.
        """
        filename = screen + '.kv'

        #clear last screen
        self.root.clear_widgets()
        
        #load new screen while making sure to unload builder
        Builder.unload_file('assets/kv/' + filename)
        screen = Builder.load_file('assets/kv/' + filename)
        self.root.add_widget(screen)
        Builder.unload_file('assets/kv/' + filename)



    def search_widgets(self, id):
        """
        Search for widgets with the specified ID.

        Args:
            id (str): ID of the widget to search for.

        Returns:
            list: List of widgets with the specified ID.
        """
        found_widgets = []

        # Check if the root widget has the specified ID
        if self.root.ids.get(id):
            found_widgets.append(self.ids[id])

        # Traverse all children widgets
        for widget in self.root.walk():
            if widget.ids.get(id):
                found_widgets.append(widget.ids[id])

        return found_widgets



    def start_game(self, game_size):
        """
        Start a new game of Katamino.

        Args:
            game_size (int): Size of the game grid.
        """

        self.ai = ai.AI(game_size)
        self.set = core.load_piece_set(randint(0,6))[:game_size]
        self.change_screen("game") # go to relevent screen
        self.game = core.Jeu(self.set, game_size) # initialize a new instance of "Jeu"

        # widget searching
        self.piece_zone = self.search_widgets("piece_zone")[0]
        self.game_zone = self.search_widgets("game_zone")[0]

        if self.game.height > 6:
            button = self.search_widgets("next_move_button")[0]
            button.opacity = 0
            button.disabled = True


        self.game.cells = [] # add a new attribute to Jeu class

        # Create and populate the grid layout representing the game_zone
        for y in range(12):
            line = BoxLayout()

            for x in range(5):
                cell = Image(source="assets/images/block.png")
                if y < 12-game_size:
                    cell.color = (0.6, 0.6, 0.6)
                else:
                    cell.id = x + (y-12+game_size)*5
                    self.game.cells.append(cell)

                line.add_widget(cell)

            self.game_zone.add_widget(line)
        

        # Add the pieces to the bank
        for piece_id in self.game.remaining_pieces:
            piece = Piece(piece_id = piece_id, app = self)
            self.piece_zone.add_widget(piece)
        

        def update_size(Window, width = Window.width, height = Window.height):
            self.game_zone.size = height*1/16 * 5 , height*1/16 * 12
        Window.bind(on_resize=update_size)

        update_size(Window)


    def path_generation(self):
        try:
            if hasattr(self.ai , "winning_path"):
                raise(ai.FoundPathException)
            
            self.ai.game = core.Jeu(self.set[:], self.game.height)
            self.ai.count = -1
            self.ai.find_winning_path(self.ai.game)

            #la pour piece placer uwus
        except ai.FoundPathException:
            if self.ai.count < len(self.ai.winning_path) -1 :
                self.ai.count += 1

                placer = self.ai.game.placer(*self.ai.winning_path[self.ai.count])
                if placer:
                    color = (randint(0, 10)/10, randint(0, 10)/10, randint(0, 10)/10)
                    for cell_id in placer:
                        self.game.cells[cell_id].color = color


class HoverBehavior:
    """
    Base class for hovering behavior.
    """
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        Window.bind(mouse_pos=self.on_mouseover)
        self.hovered = False

    def on_mouseover(self, window, pos):
        if self.collide_point(*pos):
            if not self.hovered:
                self.hovered = True
                self.on_enter()
        else:
            if self.hovered:
                self.hovered = False
                self.on_exit()

    def on_enter(self):
        pass

    def on_exit(self):
        pass

class HoverButton(Button, HoverBehavior):
    """
    Button with hover behavior.
    """
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.background_disabled_normal = "assets/images/button_disabled.png"
        self.background_normal = "assets/images/button.png"
        self.shorten = True
        self.halign = "center"

    def on_enter(self):
        self.background_normal = "assets/images/button_highlighted.png"

    def on_exit(self):
        self.background_normal = "assets/images/button.png"

class HoverSlider(Slider, HoverBehavior):
    """
    Slider with hover behavior.
    """
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.cursor_image = "assets/images/button.png"
        self.cursor_disabled_image = "assets/images/button_disabled.png"

    def on_enter(self):
        self.cursor_image = "assets/images/button_highlighted.png"

    def on_exit(self):
        self.cursor_image = "assets/images/button.png"


PIECESOFFSET = (
{"0" : (0, 4), "90" : (4, 0), "180" : (0,0), "270" : (0,0), "b0" : (0,4), "b90" : (4,0), "b180" : (0,0), "b270" : (0,0)},
{"0" : (0, 3), "90" : (3, 1), "180" : (1,0), "270" : (0,0), "b0" : (1,3), "b90" : (3,0), "b180" : (0,0), "b270" : (0,1)},
{"0" : (0, 1), "90" : (1, 3), "180" : (3,0), "270" : (0,0), "b0" : (3,1), "b90" : (1,0), "b180" : (0,0), "b270" : (0,3)},
{"0" : (0, 1), "90" : (1, 3), "180" : (3,0), "270" : (0,0), "b0" : (3,1), "b90" : (1,0), "b180" : (0,0), "b270" : (0,3)},
{"0" : (2, 2), "90" : (2, 0), "180" : (0,0), "270" : (0,2), "b0" : (0,2), "b90" : (2,2), "b180" : (2,0), "b270" : (0,0)},
{"0" : (0, 2), "90" : (2, 1), "180" : (1,0), "270" : (0,0), "b0" : (1,2), "b90" : (2,0), "b180" : (0,0), "b270" : (0,1)},
{"0" : (1, 2), "90" : (2, 0), "180" : (0,0), "270" : (0,1), "b0" : (0,2), "b90" : (2,1), "b180" : (1,0), "b270" : (0,0)},
{"0" : (2, 2), "90" : (2, 0), "180" : (0,0), "270" : (0,2), "b0" : (0,2), "b90" : (2,2), "b180" : (2,0), "b270" : (0,0)},
{"0" : (2, 1), "90" : (1, 0), "180" : (0,1), "270" : (1,2), "b0" : (0,1), "b90" : (1,2), "b180" : (2,1), "b270" : (1,0)},
{"0" : (0, 2), "90" : (2, 2), "180" : (2,0), "270" : (0,0), "b0" : (2,2), "b90" : (2,0), "b180" : (0,0), "b270" : (0,2)},
{"0" : (2, 2), "90" : (2, 0), "180" : (0,0), "270" : (0,2), "b0" : (0,2), "b90" : (2,2), "b180" : (2,0), "b270" : (0,0)},
{"0" : (1, 2), "90" : (2, 1), "180" : (1,0), "270" : (0,1), "b0" : (1,2), "b90" : (2,1), "b180" : (1,0), "b270" : (0,1)}
)


class Piece(DragBehavior, Image):
    """
    Draggable puzzle piece.
    """
    def __init__(self, piece_id, app = None, **kwargs):
        super().__init__(**kwargs)

        self.touch = 'auto_higher'
        self.cell = None
        self.orientation = "0"
        self.id = piece_id
        self.size_hint = None ,None
        self.fit_mode = "scale-down"
        self.app = app
        self.color = (randint(0, 10)/10, randint(0, 10)/10, randint(0, 10)/10)
        Window.bind(on_resize=self.update_size)

        Clock.schedule_once(lambda dt: self.update_size(), 0)
        self.rotate(self.orientation)
    
    def update_size(self, Window = Window, width = Window.width, height = Window.height):
        """
        Update the size of the piece based on the window's height.

        Args:
            window: The instance of the window.
            width: The new width of the window.
            height: The new height of the window.
        """
        self.width, self.height = self.texture_size[0]/64 * Window.height*1/16, self.texture_size[1]/64 * Window.height*1/16
        if self.cell:
            offset = PIECESOFFSET[self.id][self.orientation]
            self.x, self.y = self.cell.x - offset[0]*Window.height*1/16, self.cell.y - offset[1]*Window.height*1/16

    def rotate(self, rotation):
        """
        Rotate the piece.

        Args:
            rotation (str): Rotation value.
        """
        self.orientation = rotation
        self.source = f"""assets/images/pieces/piece{self.id}/{self.orientation}.png"""
        self.reload()
        self.update_size()

    def place(self):
        # get right cell to focus on
        cells = [cell for cell in self.app.game.cells if self.collide_widget(cell)]
        if cells:
            offset = PIECESOFFSET[self.id][self.orientation]

            piece_origin = self.x + (offset[0]+0.5)*Window.height*1/16, self.y + (offset[1]+0.5)*Window.height*1/16
            cell = min(cells,
                key=lambda cell: 
                    abs(piece_origin[0] - (cell.x + cell.width/2)) + # distance from center of piece to center of cell texture in x
                    abs(piece_origin[1] - (cell.y + cell.height/2))  # distance from center of piece to center of cell texture in y
                )
            
            # do what's needed when placing
            placer = self.app.game.placer(self.id, self.orientation, cell.id)
            if placer:
                self.cell = cell

                self.pos = cell.x - offset[0]*Window.height*1/16, cell.y - offset[1]*Window.height*1/16
                return True
        return False
    

    def on_touch_down(self, touch):
        """
        Handle touch down event.

        Args:
            touch (TouchEvent): Touch event data.
        """
        if self.collide_point(*touch.pos) and touch.button == "left":
            Window.show_cursor = False
            touch.grab(self)
            self.touch_offset = touch.x - self.x, touch.y - self.y
            self.app.current_piece = self
            self.opacity = 0.4
            

            self.parent.remove_widget(self)
            self.app.root.add_widget(self, index=0)

            # Handle piece being place or in the bank
            if self.cell:
                self.app.game.retirer(self.id, self.orientation, self.cell.id)
                self.cell = None
                
                # Update pos to match position in new parent
                self.pos = self.to_parent(*self.pos)
            return True
                    

    def on_touch_move(self, touch):
        """
        Handle touch move event.

        Args:
            touch (TouchEvent): Touch event data.
        """
        if touch.grab_current is self:
            self.pos = touch.x - self.touch_offset[0], touch.y - self.touch_offset[1]
        return True



    def on_touch_up(self, touch):
        """
        Handle touch up event.

        Args:
            touch (TouchEvent): Touch event data.
        """
        if touch.button == "left" and touch.grab_current is self:
            Window.show_cursor = True
            self.opacity = 1
            touch.ungrab(self)
            # find relevent widget
            if self.collide_widget(self.app.game_zone) and self.place():

                if self.app.game.is_won():
                    self.app.root.add_widget(
                        Popup(title='', 
                              separator_height = 0, 
                              background = "assets/images/win.png", 
                              size_hint = (0.5, 0.5), 
                              pos_hint = {"center_x" : 0.5, "center_y" : 0.5}, 
                              content=Builder.load_file("assets/kv/win.kv")))
                    Builder.unload_file("assets/kv/win.kv")
                    del self.app.game
            else:
                self.parent.remove_widget(self)
                self.app.piece_zone.add_widget(self)
        return True

            


