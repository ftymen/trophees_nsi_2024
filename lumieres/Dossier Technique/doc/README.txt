Light out

Exécution

    • accédez au répertoire du projet : projet light_out\class
    • exécutez le fichier main.py

Utilisation

Le jeu s’ouvre sur le menu avec différent choix de taille de plateau
    • utilisez le clique gauche pour interagir avec les boutons

L’interface affiche une grille de la taille sélectionnée 

Commandes :
    • utiliser le clique gauche pour interagir avec les case du plateau
    • appuyer sur la touche H pour ouvrir le menu de pause
    • appuyer sur la touche S pour résoudre le light_out seulement en longueur 4*4 et moins

Dans le menu pause :
    • cliquer sur le bouton Solution pour résoudre le light out en 4*4 et moins
    • cliquer sur le bouton Quitter pour retourner au menu principal
    • cliquer sur le bouton Reprendre pour revenir sur le plateau 

Quand le plateau et résolut l’écran de fin s’affiche
    • cliquer sur le bouton restart pour revenir au menu principal