from copy import deepcopy
from collections import deque


class File(deque):
    def __init__(self):
        super().__init__()

    def est_vide(self):
        '''renvoie True si la file est vide'''
        return len(self) == 0

    def enfiler(self, element):
        '''empile element dans la dile'''
        self.append(element)

    def defiler(self):
        '''retire le sommet de file et renvoie sa valeur'''
        if not self.est_vide():
            return self.popleft()  # retire l'élément d'index -1
        else:
            print("impossible de défiler : la file est vide!")

    def affich(self, tete_a_gauche):
        if tete_a_gauche:
            if self.est_vide():
                return "\u2190"
            return ''.join(["\u2190" + str(x) for x in self])
        if self.est_vide():
            return "\u2192"
        return ''.join([str(x) + "\u2192" for x in reversed(self)])

    def __str__(self):
        return self.affich()

    def afficher(self, tete_a_gauche=True):
        print(self.affich(tete_a_gauche))

class light_out:
    def __init__(self, cotee=5):
        self.cotee = cotee
        self.case = [[1 for i in range(cotee)] for y in range(cotee)] #cree une matrice en forme de liste de liste de 1

    def concerne(self, case):
        '''renvoie une liste avec les coordonnées de la case selectionne et ses voisines directs si elles so dans le plateau'''
        y, x = case
        lst_case = [(x, y) for (x, y) in ((x, y + 1), (x + 1, y), (x, y), (x, y - 1), (x - 1, y)) if 0 <= x < self.cotee and 0 <= y < self.cotee]
        return lst_case
    def action(self, case):
        '''change l'etat des case concernees par l'action sur une case donnee'''
        for x, y in self.concerne(case):
            self.case[x][y] = self.case[x][y] ^ 1

    def tup(self):
        '''renvoie une matrice du plateau sou forme de tuple'''
        config = tuple([tuple(ligne) for ligne in self.case])
        return config
    
    def resoudre(self): # resout pour le 3*3 et 4*4 et pas trouvé au dela
        '''renvoie une liste de tuple de coordonne a executer pour resoudre le light out'''
        resolut = tuple([tuple([0 for i in range(self.cotee)]) for y in range(self.cotee)]) #cree la matrice en forme de tuple du light out resolut
        visites = set()
        visites.add(self.tup()) #on cree un ensemble et on ajoute la configuration initial du jeu
        f = File()
        copy = deepcopy(self) #on fait un copy de l'objet
        f.enfiler(copy) #on enfile la copie de l'objet pour parcourir en largeur toutes les config du jeu
        dic_action = {copy.tup(): []} #on cree un dictionnaire avec pour cle une config du jeu et pour valeur la liste des coordonnee pour le resoudre
        while not f.est_vide():
            tete = f.defiler()
            for x in range(tete.cotee):
                for y in range(tete.cotee):
                    if (x, y) not in dic_action[tete.tup()]: #on regarde si l'action n'a pas deja ete faite sur cette case avant
                        copy_tete = deepcopy(tete)
                        copy_tete.action((x, y))
                        if copy_tete.tup() not in visites: # on regarde si la config n'a pas deja été visité et si non :
                            dic_action[copy_tete.tup()] = dic_action[tete.tup()] + [(x, y)] #on rajoute la config dans le dico et en valeur on met les coo de l'action plus ceux de sa voisine
                            f.enfiler(copy_tete)
                            visites.add(copy_tete.tup())
                    else:
                        pass
        return dic_action[resolut]