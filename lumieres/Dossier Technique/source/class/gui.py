from kivy.clock import Clock
from LightOut_core import LightsOut
from light_out_solver import light_out
from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.image import Image
from kivy.core.window import Window


class PauseScreen(Screen):
    def __init__(self, **kwargs):
        super(PauseScreen, self).__init__(**kwargs)
        layout = GridLayout(cols=1)
        
        solution_button = Button(text='Solution')
        solution_button.bind(on_release=self.show_solution)
        layout.add_widget(solution_button)
        
        quit_button = Button(text='Quitter')
        quit_button.bind(on_release=self.quit_game)
        layout.add_widget(quit_button)

        resume_button = Button(text='Reprendre')
        resume_button.bind(on_release=self.resume_game)
        layout.add_widget(resume_button)
        
        self.add_widget(layout)

    def show_solution(self, instance):
        game_screen = self.manager.get_screen('game')
        self.solution = game_screen.game.resoudre()
        print("Solution :", self.solution)

        # Crée un générateur pour parcourir la solution une action à la fois
        self.solution_generator = (action for action in self.solution)
        Clock.schedule_interval(self.perform_solution_action, 0.5)  # Exécute une action toutes les 1 secondes
        # Change l'écran actuel en 'game'
        self.manager.current = 'game'

    def perform_solution_action(self, dt):
        try:
            # Récupère la prochaine action de la solution
            action = next(self.solution_generator)
            game_screen = self.manager.get_screen('game')
            game_screen.perform_action(action)
        except StopIteration:
            # Toutes les actions de la solution ont été effectuées, donc on arrête la planification
            return False

    def quit_game(self, instance):
        self.manager.current = 'menu'

    def resume_game(self, instance):
        self.manager.current = 'game'


class MenuScreen(Screen):
    def __init__(self, **kwargs):
        super(MenuScreen, self).__init__(**kwargs)
        layout = GridLayout(cols=2)

        #Ajoute l'image en arrière-plan
        self.background = Image(source='../assets/images/Bg_jeu.jpg', allow_stretch=True, keep_ratio=False)
        self.add_widget(self.background)

        for i in range(3, 9):
            button = Button(text=f'{i}x{i}', font_size="32", size_hint=(None, None), background_color=(1, 1, 1, .7), size=(400, 40))
            button.bind(on_release=self.start_game)
            layout.add_widget(button)
            #center = BoxLayout(pos_hint= {"center_x": 0.5, "center_y": 0.5})
            #self.add_widget(center)
        self.add_widget(layout)


    def start_game(self, instance):
        size = int(instance.text[0])
        self.manager.current = 'game'
        self.manager.get_screen('game').start_game(size)

class GameScreen(Screen):
    def start_game(self, size):
        self.game = light_out(cotee=size)
        self.layout = GridLayout(cols=self.game.cotee, rows=self.game.cotee)
        for y in range(self.game.cotee):
            for x in range(self.game.cotee):
                button = Button(background_color=(255, 255, 0, 100))
                button.bind(on_release=self.switch_lights)
                self.layout.add_widget(button)
        self.add_widget(self.layout)
        
        # détection des pressions de touches
        self._keyboard = Window.request_keyboard(self._keyboard_closed, self)
        self._keyboard.bind(on_key_down=self._on_key_down)

    def switch_lights(self, instance):
        # Récupère position lumière dans la grille
        index = self.layout.children.index(instance)
        x = index % self.game.cotee
        y = index // self.game.cotee

        self.perform_action((x, y))

    def perform_action(self, coords):
        x, y = coords
        # maj état de la lumière et de c voisines
        self.game.action((x, y))

        # maj interface utilisateur
        for i, button in enumerate(self.layout.children):
            x = i % self.game.cotee
            y = i // self.game.cotee
            if self.game.case[y][x] == 1:  # Si la case est allumée
                button.background_color = (255, 255, 0, 255)  # Jaune
            else:
                button.background_color = (1, 1, 1, 1) 

        if all(val == 0 for row in self.game.case for val in row):  # Si toutes les cases sont éteintes
            self.manager.current = 'finish'
            print("Bravo")

    def _keyboard_closed(self):
        self._keyboard.unbind(on_key_down=self._on_key_down)
        self._keyboard = None

    def _on_key_down(self, keyboard, keycode, text, modifiers):
        self.manager.current = 'pause'


class FinishScreen(Screen):
    def __init__(self, **kwargs):
        super(FinishScreen, self).__init__(**kwargs)
        layout = GridLayout(cols=1)

        score_label = Label(text='Score : ' + str())
        layout.add_widget(score_label)
        
        restart_button = Button(text='Restart')
        restart_button.bind(on_release=self.quit_game)
        layout.add_widget(restart_button)


        self.add_widget(layout)


    def quit_game(self, instance):
        self.manager.current = 'menu'


    
class LightsOutApp(App):
    def build(self):
        sm = ScreenManager()
        sm.add_widget(MenuScreen(name='menu'))
        sm.add_widget(GameScreen(name='game'))
        sm.add_widget(PauseScreen(name='pause'))
        sm.add_widget(FinishScreen(name='finish'))
        return sm

if __name__ == '__main__':
    LightsOutApp().run()
