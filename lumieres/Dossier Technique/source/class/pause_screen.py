from kivy.clock import Clock
from kivy.uix.screenmanager import Screen
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button

class PauseScreen(Screen):
    def __init__(self, **kwargs):
        super(PauseScreen, self).__init__(**kwargs)
        layout = GridLayout(cols=1)
        
        solution_button = Button(text='Solution (La Solution ne fonctionne que pour le plateau de taille 3x3 et 4x4)')
        solution_button.bind(on_release=self.show_solution)
        layout.add_widget(solution_button)
        
        quit_button = Button(text='Quitter')
        quit_button.bind(on_release=self.quit_game)
        layout.add_widget(quit_button)

        resume_button = Button(text='Reprendre')
        resume_button.bind(on_release=self.resume_game)
        layout.add_widget(resume_button)
        
        self.add_widget(layout)

    def show_solution(self, instance):
        game_screen = self.manager.get_screen('game')
        self.solution = game_screen.game.resoudre()
        print("Solution :", self.solution)

        # Crée un générateur pour parcourir la solution une action à la fois
        self.solution_generator = (action for action in self.solution)
        Clock.schedule_interval(self.perform_solution_action, 0.5)  # Exécute une action toutes les 1 secondes
        # Change l'écran actuel en 'game'
        self.manager.current = 'game'

    def perform_solution_action(self, dt):
        try:
            # Récupère la prochaine action de la solution
            action = next(self.solution_generator)
            game_screen = self.manager.get_screen('game')
            game_screen.perform_action(action)
        except StopIteration:
            # Toutes les actions de la solution ont été effectuées, donc on arrête la planification
            return False

    def quit_game(self, instance):
        self.manager.current = 'menu'

    def resume_game(self, instance):
        self.manager.current = 'game'
